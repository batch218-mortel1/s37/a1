// dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.post("/registerAdmin", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

//S38 Actvity - possible answers

// A
router.post("/details", (req, res) => {
	userController.getProfileA(req.body).then(resultFromController => res.send(resultFromController));
})

// B
router.get("/details/:id", (req, res) => {
	userController.getProfileB(req.params.id).then(result => res.send(result))
});


module.exports = router;
